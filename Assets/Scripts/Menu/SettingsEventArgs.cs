﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsEventArgs : EventArgs {

    public int QualityLevel { get; set; }
}
