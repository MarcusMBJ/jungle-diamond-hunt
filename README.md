# README #

### What is this repository for? ###

* This is a repo for the game Jungle Diamond Hunt, a university project with 5 people.
* Version 1.2

## The idea from three brilliant fourth graders
The game specifications came from three young clients, tasked with coming up with their idea of a fun game 
The project intended simulate a real client-to-developer relationship, where the developers would work in an 
 agility based workflow and get continued feedback from the client during development.
 
## The Game
In Jungle Diamond Hunt the player is located in the jungle, where the goal is to find a diamond.
You will have to solve quizzes, collect food and solve puzzles. But beware, You will fall into the maze if you are not quick enough!


## Screenshots:
 ![Play menu](https://i.imgur.com/6PljdCa.jpg)
 ![Ingame](https://i.imgur.com/naekMQY.jpg)
 ![Pause](https://i.imgur.com/JmldJ7j.jpg)
 ![Ingame](https://i.imgur.com/jLtkyDl.jpg)
 ![Puzzle](https://i.imgur.com/DIZsP7e.jpg)
